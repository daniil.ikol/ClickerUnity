﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerTextBar : MonoBehaviour
{

    public Text playerNameText;
    public Text playerGoldText;
    public Text playerLevelText;
    public Text playerExpText;

    public Player player;

    public void SetPlayerNameText()
    {

        playerNameText.text = Player.playerName;
       

    }

    public void SetMenuBar()
    {
        playerLevelText.text = "Level: " + player.level.ToString();
        playerGoldText.text = "Gold: " + player.gold.ToString();
        playerExpText.text = "Exp: " + player.exp.ToString() + " / " + player.expNext.ToString();

    }

    void Update()
    {
        SetPlayerNameText();
        SetMenuBar();
    }
}
