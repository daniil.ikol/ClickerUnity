﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item
{
    private string name;
    private int price;
    private int sellPrice;
    public string Name { get => name; set => name = value; }
    public int Price { get => price; set => price = value; }
    public int GetSellPrice()
    {
        return price / 2 ;
    }

}
