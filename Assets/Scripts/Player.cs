﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Random = System.Random;



public class Player : MonoBehaviour
{
    

    public static string playerName = "Player NAME";

    public GameObject inventory;

    public string characterName = playerName;
    public int gold;
    public int exp;
    public int expNext;
    public int level;
    public int damageMin;
    public int damageMax;
    public int mana;
    public int manaMax;

    public int strength; //Damage
    public int dexterity; //Crit chance
    public int intelligence; //Mana, magic find

    private Random random = new Random();


    private void UpdateStats()
    {
        this.manaMax = this.intelligence * 10 + this.intelligence;
        this.mana = manaMax;

        this.damageMax = this.strength + this.strength * 2;
        this.damageMin = this.strength;
    }



 
    public void addExp(int exp)
    {

        this.exp += exp;

        while (this.exp >= expNext)
        {
            level++;
            this.exp -= expNext;
            this.expNext = (int)(50 / 3 * (Math.Pow(level, 3) - (6 * Math.Pow(level, 2)) + (level * 17) - 12));

            this.strength += level % 2;
            this.dexterity += level % 2;
            this.intelligence += level % 2;

            this.UpdateStats();


        }
    }

    public void AddGold(int amount)
    {
        this.gold += amount;
    }


    public int GetDamageMin()
    {
        return this.damageMin;
    }

    public int GetDamageMax()
    {
        return this.damageMax;
    }

    public int GetTotalDamage()
    {
        return random.Next(GetDamageMin(), GetDamageMax());
    }


    public int GetLevel()
    {
        return this.level;
    }

    void Update()
    {

        characterName = playerName;

        if(Input.GetKeyDown(KeyCode.I))
        {
            inventory.SetActive(!inventory.activeSelf);
        }

    }


    void Start()
    {
        SaveSystem.Init();

        inventory.SetActive(false);
        
        if(!LoadFromFile())
        {
            level = 1;
            this.exp = 0;
            this.expNext = 46;

            this.strength = 1;
            this.dexterity = 1;
            this.intelligence = 1;
            this.gold = 0;

        }


        this.UpdateStats();
    }

    public void SaveToFile()
    {
        string json = JsonUtility.ToJson(this);
        SaveSystem.Save("Character.json", json);
    }



    [Serializable]
    private class PlayerSave
    {
        public string characterName = playerName;
        public int level;
        public int gold;
        public int exp;
        public int expNext;
     

        public int strength; //Damage
        public int dexterity; //Crit chance
        public int intelligence; //Mana, magic find
    }

    public bool LoadFromFile()
    {
        string savedData = SaveSystem.Load("Character.json");

        if (savedData != null)
        {
            PlayerSave savedPlayer = JsonUtility.FromJson<PlayerSave> (savedData);

            playerName = savedPlayer.characterName;
            this.level = savedPlayer.level;
            this.gold = savedPlayer.gold;
            this.exp = savedPlayer.exp;
            this.expNext = savedPlayer.expNext;
            this.strength = savedPlayer.strength;
            this.dexterity = savedPlayer.dexterity;
            this.intelligence = savedPlayer.intelligence;

            return true;

        }

        return false;

    }

    void OnApplicationQuit()
    {
        SaveToFile();
    }

}
