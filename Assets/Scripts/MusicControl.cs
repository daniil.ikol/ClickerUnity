﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MusicControl : MonoBehaviour
{


    public AudioSource audio;
    static MusicControl instance = null;


    void Awake()
    {

        if (instance != null) { Destroy(gameObject); }
        else { instance = this; DontDestroyOnLoad(gameObject); }

    }


    void Update()
    {
        audio.volume = VolumeChange.VOLUME;
    }
}
