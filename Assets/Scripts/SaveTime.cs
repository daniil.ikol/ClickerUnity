﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SaveTime : MonoBehaviour
{
    [Serializable]
    private class SavedTime
    {
        public int CurrentTime;
    }

    public int CurrentTime;

    public int getCurrentTime()

    {

        DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Local);//from 1970/1/1 00:00:00 to now

        DateTime dtNow = DateTime.Now;

        TimeSpan result = dtNow.Subtract(dt);

        int seconds = Convert.ToInt32(result.TotalSeconds);


        return seconds;

    }

    void OnApplicationQuit()
    {
        SaveCurrentTimeToFile();
    }



    public void SaveCurrentTimeToFile()
    {
        string json = JsonUtility.ToJson(this);
        SaveSystem.Save("OfflineTime.json",json);
    }


    void Update()
    {

        this.CurrentTime = getCurrentTime();

    }

}
