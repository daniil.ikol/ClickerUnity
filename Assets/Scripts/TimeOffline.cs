﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class TimeOffline : MonoBehaviour
{
    private class Time
    {
        public int CurrentTime;
    }

    public Text changeText;

    public int CurrentTime;
    private int LastOnlineTime;

    public int getCurrentTime()

    {

        DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Local);//from 1970/1/1 00:00:00 to now

        DateTime dtNow = DateTime.Now;

        TimeSpan result = dtNow.Subtract(dt);

        int seconds = Convert.ToInt32(result.TotalSeconds);


        return seconds;

    }


    public void SaveCurrentTimeToFile()
    {

        string json = JsonUtility.ToJson(this);
        SaveSystem.Save("OfflineTime.json", json);

    }

    public void LoadTimeFromFile()
    {
        string savedTime = SaveSystem.Load("OfflineTime.json");

        if (savedTime != null)
        {
            Time time = JsonUtility.FromJson<Time>(savedTime);

            LastOnlineTime = time.CurrentTime;
        }
    }


    void OnApplicationQuit()
    {
        SaveCurrentTimeToFile();
    }

    public int getEscapedTime()
    {

        int Escapedtime = CurrentTime - LastOnlineTime;

        return Escapedtime;
    }

    public string getTimeAsStringDays(int escapedTime)
    {

        TimeSpan time = TimeSpan.FromSeconds(escapedTime);

       
         return time.ToString(@"dd");
    }

    public string getTimeAsStringHours(int escapedTime)
    {

        TimeSpan time = TimeSpan.FromSeconds(escapedTime);


        return time.ToString(@"hh\:mm");
    }

    public string getTimeAsStringSeconds(int escapedTime)
    {

        TimeSpan time = TimeSpan.FromSeconds(escapedTime);


        return time.ToString(@"ss");
    }

    public string FinalEscapedTime()
    {
       StringBuilder escapedTime = new StringBuilder("You were offline: ");

        if (!(getTimeAsStringDays(getEscapedTime())).Contains("00"))
        {
            escapedTime.Append(getTimeAsStringDays(getEscapedTime()) + " Days");

            if (!(getTimeAsStringHours(getEscapedTime())).Contains("00"))
            {
                escapedTime.Append(" " + getTimeAsStringHours(getEscapedTime()) + " Hours");
            }
        }
        else if (!(getTimeAsStringHours(getEscapedTime())).Contains("00"))
        {
            escapedTime.Append(" " + getTimeAsStringHours(getEscapedTime()) + " Hours");
        }
        else if (!(getTimeAsStringSeconds(getEscapedTime())).Contains("00"))
        {
            escapedTime.Append(getTimeAsStringSeconds(getEscapedTime()) + " Seconds");
        }
        else {
            escapedTime.Replace("You were offline: ", "You're playing now");

        }
       


        return escapedTime.ToString();
    }

 

    // Start is called before the first frame update
    void Start()
    {
        SaveSystem.Init();
        LoadTimeFromFile();
        CurrentTime = getCurrentTime();

        changeText.text = FinalEscapedTime();
    }

    // Update is called once per frame
    void Update()
    {
        CurrentTime = getCurrentTime();
        
    }
}
