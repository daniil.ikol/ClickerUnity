﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeChange : MonoBehaviour
{

    public Slider slider;
    public static float VOLUME = 0.5f;




    public void updateVolume(float value)
    {
        VOLUME = value;

    }

    void Start()
    {
        slider.value = VOLUME;
    }
}
