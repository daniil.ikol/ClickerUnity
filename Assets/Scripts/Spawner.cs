﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public static bool isNeedSpawn = true;
    public Player player;
    public GameObject enemy;


    void Awake()
    {
           isNeedSpawn = true;
        
        if (isNeedSpawn)
        {
            Instantiate(enemy, new Vector3(400, 100, 0), Quaternion.identity, GameObject.FindGameObjectWithTag("SettingCanvas").transform);
            isNeedSpawn = false;
        }
    }  

    // Update is called once per frame
    void Update()
    {
        
        if (isNeedSpawn)
        {
            Instantiate(enemy, new Vector3(400, 100, 0), Quaternion.identity, GameObject.FindGameObjectWithTag("SettingCanvas").transform); 
            isNeedSpawn = false;
        }
    }
}
