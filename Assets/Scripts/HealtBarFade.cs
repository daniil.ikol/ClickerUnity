﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealtBarFade : MonoBehaviour
{

    private const float DAMAGED_HEALTH_FADE_TIMER_MAX = .6f;

    private Image barImage;
    private Image damageBarImage;
    private Color damageColor;
    private float damageHeathFadeTimer;


     void Awake()
    {
        barImage = transform.Find("bar").GetComponent<Image>();
        damageBarImage = transform.Find("damagedBar").GetComponent<Image>();
        damageColor = damageBarImage.color;
        damageColor.a = 0f;
        damageBarImage.color = damageColor;

    }

     void Update()
    {
        if (damageColor.a > 0)
        {
            damageHeathFadeTimer -= Time.deltaTime;
            if( damageHeathFadeTimer < 0)
            {
                float fadeAmount = 5f;
                damageColor.a -= fadeAmount * Time.deltaTime;
                damageBarImage.color = damageColor;
            }
        }
    }


    void Start()
    {
        SetHealth(1f);
    }

 
    public void SetHealth(float healtNormalized)
    {
        barImage.fillAmount = healtNormalized;
    }
}
