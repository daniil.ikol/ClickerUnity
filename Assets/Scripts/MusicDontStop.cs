﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicDontStop : MonoBehaviour
{
    public AudioSource bgMusic;

    void Start() {
        bgMusic.volume = PlayerPrefs.GetFloat("MusicVolume");
    }
    void Awake() {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("BackgroundMusic");
        if (objs.Length > 1)
            Destroy(this.gameObject);
        DontDestroyOnLoad(this.gameObject);
    }
}
