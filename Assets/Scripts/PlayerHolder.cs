﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHolder : MonoBehaviour
{
    // Declare any public variables that you want to be able 
    // to access throughout your scene

    public HealtBarFade healtBar;
    public Player player;
    public static PlayerHolder Instance { get; private set; } // static singleton
    void Awake()
    {
        if (Instance == null) { Instance = this; }
        else { Destroy(gameObject); }
        // Cache references to all desired variables
        player = FindObjectOfType<Player>();
        healtBar = FindObjectOfType<HealtBarFade>();
    }

    void Update()
    {
        player = FindObjectOfType<Player>();
        healtBar = FindObjectOfType<HealtBarFade>();
    }



}
