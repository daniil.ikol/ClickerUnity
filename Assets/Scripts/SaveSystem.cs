﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveSystem
{

    public static readonly string SAVE_FOLDER = Application.dataPath + "/Saves/";

    public static void Init()
    {
        //Test if folder exist
        if (!Directory.Exists(SAVE_FOLDER))
        {
            Directory.CreateDirectory(SAVE_FOLDER);
        }
    }

    public static void Save(string SAVE_FILE, string saveString)
    {
        File.WriteAllText(SAVE_FOLDER+ SAVE_FILE, saveString);
    }

    public static string Load(string SAVE_FILE)
    {
        if (File.Exists(SAVE_FOLDER + SAVE_FILE))
        {
            string saveString = File.ReadAllText(SAVE_FOLDER + SAVE_FILE);
            return saveString;
        }
        else {
            return null;
        }
    }

}
