﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Enemy : MonoBehaviour, IPointerClickHandler
{
    public Player player;
    public HealtBarFade healtBarFade;


    private int level;
    private int hp;
    private int hpMax;


    void Start()
    {
        player = PlayerHolder.Instance.player;
        healtBarFade = PlayerHolder.Instance.healtBar;
        healtBarFade.SetHealth(1f);
        level = player.GetLevel();
        this.hpMax = this.level * 10 + this.level;
        this.hp = this.hpMax;
  
    }

    public float GetHealthNormalized()
    {
        return (float)hp / hpMax;
    }

    public void takeDamage(int damage)
    {
        this.hp -= damage;

        if (this.hp <= 0)
            setDead();

    }

    public void setDead()
    {
        this.hp = 0;

        this.player.addExp(this.level * 2);
        this.player.AddGold(this.level * 2);

        Spawner.isNeedSpawn = true;
        Destroy(this.gameObject);   
    }


    public bool isAlive()
    {
        return this.hp <= 0;
    }


    public void OnPointerClick(PointerEventData eventData)
    {
        if (!isAlive())
        {
            takeDamage(player.GetTotalDamage());
            healtBarFade.SetHealth(GetHealthNormalized());
        }
        
    }
}
